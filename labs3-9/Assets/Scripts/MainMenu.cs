﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public static readonly string GameScene = "GameScene";

    public void OnCreatePlayScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(GameScene);
    }

    public void OnExit()
    {
        Application.Quit();
    }

    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
}
